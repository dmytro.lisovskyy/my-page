function onCheckBtnClick() {
    const resElement = document.getElementById('result');
    const inputElement = document.getElementById('word');
    let result = inputElement && isIsogram(inputElement.value);

    if (resElement) {

        resElement.innerHTML = result ? 'Ізограм' : 'Не ізограм';
    }
}


function isIsogram(str) {
    const lowerCase = str.toLowerCase();
    const arr = lowerCase.split("")

    for (let i = 0; i < arr.length; i++) {
    if (lowerCase.indexOf(arr[i], i+1) >= 0 ) {
        return false;
    }

    }
    return true;

}

isIsogram(prompt("enter any word"));
