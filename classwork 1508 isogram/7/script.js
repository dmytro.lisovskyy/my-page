function buildTower(countOfFloors) {

    const numberOfBricks = 2 * countOfFloors - 1;
    result = [];
    for (let i = 0; i < countOfFloors; i++) {
        result.push(buildFloor( i + 1, numberOfBricks))
    }
    return result;

}
function buildFloor(currentFloor, numberOfBricks) {
    let result = "";

        const spaces = (numberOfBricks -1) / 2;
        const stars = (2 * currentFloor) - 1;

        return " ".repeat(spaces) + "*".repeat(stars) + " ".repeat(spaces);

}