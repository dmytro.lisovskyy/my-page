const mainBtn = document.createElement('button');
mainBtn.setAttribute('type', 'button');
mainBtn.innerHTML = 'The best of the best';

mainBtn.addEventListener('click', () => {
    for(let i = 0; i < 50; i++) {
        const btn = document.createElement('button');
        btn.setAttribute('type', 'button');
        btn.innerHTML = i+1;
        btn.addEventListener('click', function() {
            alert(i+1);
        });
        document.body.append(btn);
    }
});

document.body.append(mainBtn);
