const PRODUCTS = [
    {
        title: 'Ноутбук Apple MacBook Air 13" 256GB 2020 Space Gray (MWTJ2)',
        description: 'Екран 13.3" IPS (2560x1600), глянсовий / Intel Core i3 (1.1 — 3.2 ГГц) / RAM 8 ГБ / SSD 256 ГБ / Intel Iris Plus Graphics / Wi-Fi / Bluetooth / macOS Catalina / 1.29 кг / сірий',
        price: 29999,
        popularIndex: 0.7,
        viewed: 1596285861067 //Date.now()
    },
    {
        title: "Мобильный телефон Apple iPhone 11 64GB",
        description: 'Экран (6.1", IPS (Liquid Retina HD), 1792x828)/ Apple A13 Bionic/ основная двойная камера: 12 Мп + 12 Мп, фронтальная камера: 12 Мп/ RAM 4 ГБ/ 64 ГБ встроенной памяти/ 3G/ LTE/ GPS/ ГЛОНАСС/ Nano-SIM/ iOS 13 / 3046 мА*ч',
        price: 1999,
        popularIndex: 0.2,
        viewed: 1596285861082
    },
    {
        title: "Мобильный телефон Apple iPhone 8",
        description: 'Э основная двойная камера: 12 Мп + 12 Мп, фронтальная камера: 12 Мп/ RAM 4 ГБ/ 64 ГБ встроенной памяти/ 3G/ LTE/ GPS/ ГЛОНАСС/ Nano-SIM/ iOS 13 / 3046 мА*ч',
        price: 1999,
        popularIndex: 0.3,
        viewed: 1596285861082


    },

    {
        title: "Мобильный телефон Apple iPhone 5",
        description: '12 Мп/ RAM 4 ГБ/ 64 ГБ встроенной памяти/ 3G/ LTE/ GPS/ ГЛОНАСС/ Nano-SIM/ iOS 13 / 3046 мА*ч',
        price: 1999,
        popularIndex: 0.4,
        viewed: 1596285861082

    },
    {
        title: "Мобильный телефон Apple iPhone 4",
        description: '12 Мп/ RAM 4 ГБ/ 64 ГБ встроенной памяти/ 3G/ LTE/ GPS/ ГЛОНАСС/ Nano-SIM/ iOS 13 / 3046 мА*ч',
        price: 1999,
        popularIndex: 0.4,
        viewed: 1596285861082
    }

];

