const list = getPopularProducts(PRODUCTS);


const html = '<ul>' + (list.map(product => {
    return `<li><div><img src="${product.img}"></div>${product.title}</li>`
}).join('')) + '</ul>';


document.write(html);
