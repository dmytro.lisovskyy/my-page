let first_name = undefined;
let last_name = undefined;
let middle_name = undefined;

function somePerson(first_name, last_name, middle_name:string = "") {
    return {
        first_name: first_name,
        last_name: last_name,
        middle_name: middle_name,

        getShortName: function () {
            let str = this.last_name || "no lastname";

            if (this.first_name) {
                str += "" + this.first_name.charAt(0).toUpperCase() + ".";

            }

            if (this.middle_name) {
                str += "" + this.middle_name.charAt(0)            }
            // return `${this.last_name} ${this.first_name} ${this.middle_name }`

        }
    }


}